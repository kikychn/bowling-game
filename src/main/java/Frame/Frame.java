package Frame;

public class Frame {
    private int firstThrow = 0;
    private int secondThrow = 0;
    private boolean isSpare = false;
    private boolean isStrike = false;

    public Frame(int firstThrow) {
        this(firstThrow, 0);
    }

    public Frame(int firstThrow, int secondThrow) {
        this.firstThrow = firstThrow;
        this.secondThrow = secondThrow;

        if (firstThrow == 10) {
            isStrike = true;
        } else if (firstThrow + secondThrow == 10) {
            isSpare = true;
        }
    }

    public boolean isStrike() {
        return isStrike;
    }

    public boolean isSpare() {
        return isSpare;
    }

    public int getFirstThrow() {
        return firstThrow;
    }

    public int getSecondThrow() {
        return secondThrow;
    }

    public int getTotalThrows() {
        return firstThrow + secondThrow;
    }
}

package Game;

import Frame.Frame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Game {
    private List<Frame> frames = new ArrayList<>();
    private final int maxFrameCount = 10;
    private int frameCount = 0;

    public Game() {}

    public Game(Frame[] frames) {
        this.addFrames(frames);
    }

    public Game(List<Frame> frames) {
        this.addFrames(frames);
    }

    public void addFrame(Frame frame) {
        frames.add(frame);
        frameCount = Math.min(maxFrameCount, frames.size());
    }

    public void addFrames(List<Frame> frames) {
        this.frames.addAll(frames);
        frameCount = Math.min(maxFrameCount, this.frames.size());
    }

    public void addFrames(Frame[] frames) {
        this.addFrames(Arrays.asList(frames));
    }

    public int getScore() {
        if (frames.size() == 0) {
            return 0;
        }
        return getBaseScore() + getBonus();
    }

    private int getBaseScore() {
        return frames.stream().limit(frameCount).mapToInt(frame -> frame.getTotalThrows()).sum();
    }

    private int getBonus() {
        int bonus = 0;
        for (int i = 0; i < frameCount; i++) {
            if (frames.get(i).isSpare()) {
                bonus += getSpareBonus(i);
            } else if (frames.get(i).isStrike()) {
                bonus += getStrikeBonus(i);
            }
        }
        return bonus;
    }

    private int getStrikeBonus(int index) {
        int strikeBonus = 0;
        if (frames.get(index + 1).isStrike()) {
            strikeBonus =
                    frames.get(index + 1).getFirstThrow() + frames.get(index + 2).getFirstThrow();
        } else {
            strikeBonus = frames.get(index + 1).getTotalThrows();
        }
        return strikeBonus;
    }

    private int getSpareBonus(int index) {
        return frames.get(index + 1).getFirstThrow();
    }
}

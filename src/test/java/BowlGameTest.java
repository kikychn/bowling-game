import Frame.Frame;
import Game.Game;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BowlGameTest {

    private Game game;
    private Frame frameIsNotStrikeOrSpare;
    private Frame spareFrame;
    private Frame strikeFrame;

    @BeforeEach
    void setUp() {
        game = new Game();
        frameIsNotStrikeOrSpare = new Frame(3, 6);
        spareFrame = new Frame(4, 6);
        strikeFrame = new Frame(10);
    }

    @Test
    void should_get_0_when_no_throw() {
        assertEquals(0, game.getScore());
    }

    @Test
    void should_get_score_when_1_throw() {
        game.addFrame(new Frame(6));
        assertEquals(6, game.getScore());
    }

    @Test
    void should_get_score_when_2_throw() {
        game.addFrame(new Frame(3, 6));
        assertEquals(9, game.getScore());
    }

    @Test
    void should_get_score_when_10_frames_without_strike_and_spare() {
        addFramesWithoutStrikeAndSpare(10);
        assertEquals(90, game.getScore());
    }

    private void addFramesWithoutStrikeAndSpare(int frameTimes) {
        Stream.iterate(0, i -> i + 1)
                .limit(frameTimes)
                .forEach(i -> game.addFrame(frameIsNotStrikeOrSpare));
    }

    @Test
    void should_get_score_when_10_frames_with_only_first_frame_is_spare_and_no_strike() {
        addSpareFrames(1);
        addFramesWithoutStrikeAndSpare(9);
        assertEquals(94, game.getScore());
    }

    private void addSpareFrames(int frameTimes) {
        Stream.iterate(0, i -> i + 1).limit(frameTimes).forEach(i -> game.addFrame(spareFrame));
    }

    @Test
    void should_get_score_when_10_frames_without_strike_and_the_last_frame_is_not_spare() {
        addSpareFrames(1);
        addFramesWithoutStrikeAndSpare(3);
        addSpareFrames(2);
        addFramesWithoutStrikeAndSpare(4);

        assertEquals(103, game.getScore());
    }

    @Test
    void should_get_score_when_10_frames_without_strike_and_the_last_frame_is_spare() {
        addSpareFrames(1);
        addFramesWithoutStrikeAndSpare(3);
        addSpareFrames(2);
        addFramesWithoutStrikeAndSpare(3);
        addSpareFrames(1);
        game.addFrame(new Frame(4));

        assertEquals(108, game.getScore());
    }

    @Test
    void should_get_score_when_10_frames_with_only_first_is_strike() {
        addStrikeFrames(1);
        addFramesWithoutStrikeAndSpare(3);
        addSpareFrames(2);
        addFramesWithoutStrikeAndSpare(3);
        addSpareFrames(1);
        game.addFrame(new Frame(4));

        assertEquals(114, game.getScore());
    }

    private void addStrikeFrames(int frameTimes) {
        Stream.iterate(0, i -> i + 1).limit(frameTimes).forEach(i -> game.addFrame(strikeFrame));
    }

    @Test
    void should_get_score_when_10_frames_with_last_is_not_strike() {
        addStrikeFrames(1);
        addFramesWithoutStrikeAndSpare(3);
        addSpareFrames(2);
        addFramesWithoutStrikeAndSpare(1);
        addStrikeFrames(2);
        addSpareFrames(1);
        game.addFrame(new Frame(4));

        assertEquals(140, game.getScore());
    }

    @Test
    void should_get_score_when_10_frames_with_last_is_strike() {
        addStrikeFrames(1);
        addFramesWithoutStrikeAndSpare(3);
        addSpareFrames(2);
        addFramesWithoutStrikeAndSpare(2);
        addSpareFrames(1);
        addStrikeFrames(1);
        game.addFrame(new Frame(4, 6));

        assertEquals(131, game.getScore());
    }

    @Test
    void should_get_perfect_score_when_all_frames_are_strike() {
        addStrikeFrames(12);
        assertEquals(300, game.getScore());
    }

    @Test
    void should_add_frames_list_to_game() {
        List<Frame> frames = new ArrayList<>();
        frames.add(new Frame(3, 6));
        frames.add(new Frame(3, 6));
        frames.add(new Frame(3, 6));
        frames.add(new Frame(4, 6));
        frames.add(new Frame(10));
        frames.add(new Frame(3, 6));
        frames.add(new Frame(3, 6));
        game.addFrames(frames);

        assertEquals(84, game.getScore());
    }

    @Test
    void should_add_frames_array_to_game() {
        Frame[] frames = new Frame[3];
        frames[0] = (new Frame(10));
        frames[1] = (new Frame(4, 6));
        frames[2] = (new Frame(3, 6));
        game.addFrames(frames);

        assertEquals(42, game.getScore());
    }

    @Test
    void should_create_new_game_using_constructor_with_array() {
        Frame[] frames = new Frame[3];
        frames[0] = (new Frame(10));
        frames[1] = (new Frame(4, 6));
        frames[2] = (new Frame(3, 6));
        Game newGame = new Game(frames);

        assertEquals(42, newGame.getScore());
    }

    @Test
    void should_create_new_game_using_constructor_with_list() {
        List<Frame> frames = new ArrayList<>();
        frames.add(new Frame(3, 6));
        frames.add(new Frame(3, 6));
        frames.add(new Frame(3, 6));
        frames.add(new Frame(4, 6));
        frames.add(new Frame(10));
        frames.add(new Frame(3, 6));
        frames.add(new Frame(3, 6));
        Game newGame = new Game(frames);

        assertEquals(84, newGame.getScore());
    }
}
